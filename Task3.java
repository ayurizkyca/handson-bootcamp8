import java.util.Random;
import java.util.Scanner;

public class Task3 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        Random random = new Random();

        boolean playAgain = true;

        while (playAgain) {
            int secretNumber = random.nextInt(100) + 1;
            int guess;
            int numberOfGuesses = 0;

            System.out.println("Guess the Number game (1-100)!");

            do {
                System.out.print("Input Number: ");
                guess = scanner.nextInt();
                numberOfGuesses++;

                if (guess < secretNumber) {
                    System.out.println("The number is too small. Try again.");
                } else if (guess > secretNumber) {
                    System.out.println("The number is too large. Try again.");
                } else {
                    System.out.println("Congratulations! You guessed the number " + secretNumber + ".");
                    System.out.println("Number of guesses: " + numberOfGuesses);

                    System.out.print("Do you want to play again? (y/n): ");
                    String answer = scanner.next().toLowerCase();

                    if (answer.equals("n")) {
                        playAgain = false;
                    }

                    break;
                }
            } while (true);
        }

        System.out.println("Thank you for playing!");
        scanner.close();
    }
}
