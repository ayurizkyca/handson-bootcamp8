import java.util.Scanner;

public class Task1 {
    public static void main(String[] args) {
        Scanner keyboard = new Scanner(System.in);

        double resistance, current, voltage, power;

        System.out.println("Watts-Volts-Amp-Ohms Calculator");
        System.out.print("Enter Resistance (R) value: ");
        resistance = keyboard.nextDouble();

        System.out.print("Enter Current (I) value: ");
        current = keyboard.nextDouble();

        System.out.print("Enter Voltage (V) value: ");
        voltage = keyboard.nextDouble();

        System.out.print("Enter Power (P) value: ");
        power = keyboard.nextDouble();

        if (resistance == 0) {
            resistance = (current != 0) ? voltage / current : (power != 0) ? Math.pow(voltage, 2) / power : 0;
            System.out.println("Calculated Resistance (R): " + resistance);
        } else if (current == 0) {
            current = (voltage!=0 && resistance !=0) ? voltage / resistance : (power != 0) ? Math.sqrt(power/resistance) : (resistance == 0) ? power*voltage : 0;
            System.out.println("Calculated Current (I): " + current);
        } else if (voltage == 0) {
            voltage = current * resistance;
            System.out.println("Calculated Voltage (V): " + voltage);
        } else if (power == 0) {
            power = voltage * current;
            System.out.println("Calculated Power (P): " + power);
        }

        System.out.println("===Result===");
        System.out.println("Resistance (R): " + resistance);
        System.out.println("Current (I): " + current);
        System.out.println("Voltage (V): " + voltage);
        System.out.println("Power (P): " + power);

        keyboard.close();
    }
}

