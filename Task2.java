import java.time.LocalDate;
import java.time.Period;
import java.util.Scanner;

public class Task2 {
    public static void main(String[] args) {
        // Membaca input tanggal lahir dari pengguna
        Scanner keyboard = new Scanner(System.in);
        System.out.print("Input Your Birthday (YYYY-MM-DD): ");
        String inputBirthday = keyboard.nextLine();

        // Mengonversi input menjadi objek LocalDate
        LocalDate birthday = LocalDate.parse(inputBirthday);

        // Menghitung umur menggunakan kelas Period
        Period age = Period.between(birthday, LocalDate.now());

        // Menampilkan hasil
        System.out.println("Your Age...");
        System.out.println("Year : " + age.getYears());
        System.out.println("Month : " + age.getMonths());
        System.out.println("Day : " + age.getDays());

        // Menutup keyboard
        keyboard.close();
    }
}
